import random
from typing import List, Union
import streamlit as st
import numpy as np
import cv2
import albumentations as A


st.set_page_config(
    page_title="Image augmentations", layout="centered"
)


st.title("Image augmentations")
st.markdown("---")


def render_image(
        image: np.ndarray, sub_header: str
):
    """
    Render an image
    :param image:
    :param sub_header:
    :return:
    """
    st.subheader(sub_header)
    with st.columns(3)[1]:
        st.image(image=image)


def render_image_with_mask(
        image: np.ndarray, mask: np.ndarray, sub_header: str
):
    """
    Render an image with mask
    :param image:
    :param mask:
    :param sub_header:
    :return:
    """
    tmp = np.zeros_like(image)
    for value in np.unique(mask):
        tmp[mask == value] = [
            random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)
        ]

    tmp = np.concatenate(
        [image, tmp], axis=1
    )
    st.subheader(sub_header)
    st.image(image=tmp)


def render_image_with_bboxes(
        image: np.ndarray, bboxes: List[List[Union[int, float]]],
        labels: List[str], sub_header: str
):
    """
    Render image with bboxes
    :param image:
    :param bboxes:
    :param labels:
    :param sub_header:
    :return:
    """
    bboxes: List[List[int]] = [
        [int(x) for x in bbox]
        for bbox in bboxes
    ]
    tmp = image.copy()
    for bbox, label in zip(bboxes, labels):
        cv2.rectangle(
            img=tmp, pt1=bbox[:2], pt2=bbox[2:], color=(0, 255, 0), thickness=3
        )
        cv2.putText(
            text=label, img=tmp, org=[bbox[0], bbox[1] - 5], color=(0, 255, 0), thickness=3,
            fontScale=2.0, fontFace=cv2.FONT_HERSHEY_PLAIN
        )
    st.subheader(sub_header)
    st.image(image=tmp)


st.header("Augmentations for classification")

origin_image: np.ndarray = cv2.imread(
    filename="data/classification_image.jpg", flags=cv2.IMREAD_COLOR
)
origin_image = cv2.cvtColor(src=origin_image, code=cv2.COLOR_BGR2RGB)
render_image(image=origin_image, sub_header="Origin image")

horizontal_flip_image = A.HorizontalFlip(p=1.0)(image=origin_image)["image"]
render_image(image=horizontal_flip_image, sub_header="Horizontal flip")

gaussian_image = A.GaussianBlur(blur_limit=(21, 31), p=1.0)(image=origin_image)["image"]
render_image(image=gaussian_image, sub_header="Gaussian blur")

hsv_image = A.HueSaturationValue(
    hue_shift_limit=50, sat_shift_limit=50, val_shift_limit=50, p=1.0
)(image=origin_image)["image"]
render_image(image=hsv_image, sub_header="HSV change")

gray_image = A.ToGray(p=1.0)(image=origin_image)["image"]
render_image(image=gray_image, sub_header="To gray")


st.header("Augmentations for segmentation")

origin_image: np.ndarray = cv2.imread(
    filename="data/segment_image.jpg", flags=cv2.IMREAD_COLOR
)
origin_image = cv2.cvtColor(src=origin_image, code=cv2.COLOR_BGR2RGB)
origin_mask: np.ndarray = cv2.imread(
    filename="data/segment_mask.png", flags=cv2.IMREAD_GRAYSCALE
)
render_image_with_mask(image=origin_image, mask=origin_mask, sub_header="Origin image")

random_crop = A.RandomCrop(height=200, width=300, p=1.0)(image=origin_image, mask=origin_mask)
render_image_with_mask(
    image=random_crop["image"], mask=random_crop["mask"], sub_header="Random crop"
)

pixel_dropout = A.PixelDropout(dropout_prob=0.05, p=1.0)(image=origin_image, mask=origin_mask)
render_image_with_mask(
    image=pixel_dropout["image"], mask=pixel_dropout["mask"], sub_header="Pixel dropout"
)

coarse_dropout = A.CoarseDropout(max_height=50, max_width=50, p=1.0)(image=origin_image, mask=origin_mask)
render_image_with_mask(
    image=coarse_dropout["image"], mask=coarse_dropout["mask"], sub_header="Coarse dropout"
)

grid_shuffle = A.RandomGridShuffle(grid=(3, 3), p=1.0)(image=origin_image, mask=origin_mask)
render_image_with_mask(
    image=grid_shuffle["image"], mask=grid_shuffle["mask"], sub_header="Grid shuffle"
)

grid_dropout_data = A.GridDropout(p=1.0)(image=origin_image, mask=origin_mask)
render_image_with_mask(
    image=grid_dropout_data["image"], mask=grid_dropout_data["mask"],
    sub_header="Grid dropout"
)


st.header("Augmentations for detection")

origin_image = cv2.imread(
    filename="data/detection_image.jpeg", flags=cv2.IMREAD_COLOR
)
origin_image = cv2.cvtColor(src=origin_image, code=cv2.COLOR_BGR2RGB)
origin_bboxes = [
    [1, 172, 107, 282],
    [180, 386, 487, 687],
    [222, 67, 376, 206],
    [343, 702, 566, 768],
    [450, 480, 700, 625],
    [503, 57, 565, 196],
    [755, 49, 875, 130],
    [651, 340, 781, 414],
    [681, 550, 845, 705],
    [792, 702, 1014, 768]
]
origin_labels: List[str] = [
    "Type_1", "Type_2", "Type_2", "Type_3", "Type_3",
    "Type_4", "Type_1", "Type_1", "Type_2", "Type_3"
]
render_image_with_bboxes(
    image=origin_image, bboxes=origin_bboxes, labels=origin_labels,
    sub_header="Origin image"
)

crop_data = A.Compose(
    transforms=[
        A.CenterCrop(
            height=500, width=500, p=1
        )
    ],
    bbox_params=A.BboxParams(
        format="pascal_voc", label_fields=["labels"], min_visibility=0.5
    )
)(image=origin_image, bboxes=origin_bboxes, labels=origin_labels)
render_image_with_bboxes(
    image=crop_data["image"], bboxes=crop_data["bboxes"], labels=crop_data["labels"],
    sub_header="Center crop"
)

resize_data = A.Compose(
    transforms=[
        A.Resize(height=400, width=800, p=1.0)
    ],
    bbox_params=A.BboxParams(
        format="pascal_voc", label_fields=["labels"], min_visibility=0.5
    )
)(image=origin_image, bboxes=origin_bboxes, labels=origin_labels)
render_image_with_bboxes(
    image=resize_data["image"], bboxes=resize_data["bboxes"], labels=resize_data["labels"],
    sub_header="Resize"
)

affine_data = A.Compose(
    transforms=[
        A.Affine(
            scale=0.9, translate_percent=0.1, rotate=45, p=1.0
        )
    ],
    bbox_params=A.BboxParams(
        format="pascal_voc", label_fields=["labels"], min_visibility=0.5
    )
)(image=origin_image, bboxes=origin_bboxes, labels=origin_labels)
render_image_with_bboxes(
    image=affine_data["image"], bboxes=affine_data["bboxes"], labels=affine_data["labels"],
    sub_header="Affine"
)
